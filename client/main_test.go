package main

import (
	"reflect"
	"testing"
)

var (
	responseGET = []Student{
		{ID: "B001", Name: "Jason Bourne", Age: 29, Grade: 1},
		{ID: "B002", Name: "James Bond", Age: 27, Grade: 1},
		{ID: "E001", Name: "Ethan Hunt", Age: 27, Grade: 2},
		{ID: "W001", Name: "John Wick", Age: 28, Grade: 2},
		{ID: "L001", Name: "Leslie Pham", Age: 21, Grade: 3},
		{ID: "X001", Name: "Xavier Johann", Age: 14, Grade: 3},
	}
	responsePOST = Response{
		StatusCode: 400,
		Message:    "ID already exist!",
		Success:    false,
	}
	responseDELETE = Response{
		StatusCode: 400,
		Message:    "Bad Request, ID does not exist!",
		Success:    false,
	}
)

var newStudent = Student{
	ID:    "L001",
	Name:  "Leslie Pham",
	Age:   21,
	Grade: 3,
}

func TestFetchStudents(t *testing.T) {
	students, _ := fetchStudents()
	t.Logf("response GET: %+v ", students)
	if reflect.DeepEqual(students, responseGET) != true {
		t.Errorf("Wrong Values! should be %+v", responseGET)
	}
}

func TestAddStudent(t *testing.T) {
	resp, _ := addStudent(newStudent)
	t.Logf("response GET: %+v ", resp)
	if reflect.DeepEqual(resp, responsePOST) != true {
		t.Errorf("Wrong Values! should be %+v", responsePOST)
	}
}

func TestRemoveStudent(t *testing.T) {
	var studentID = "C001"
	resp, _ := removeStudent(studentID)
	t.Logf("response DELETE: %+v ", resp)
	if reflect.DeepEqual(resp, responseDELETE) != true {
		t.Errorf("Wrong Values! should be %+v", responseDELETE)
	}
}
