package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
)

var baseURL = "http://localhost:8080"

// Student is exported
type Student struct {
	ID    string `json:"id"`
	Name  string `json:"name"`
	Age   int    `json:"age"`
	Grade int    `json:"grade"`
}

// Response is exported
type Response struct {
	Success    bool   `json:"success"`
	Message    string `json:"message"`
	StatusCode int    `json:"error_code"`
}

// Response is exported
type fetchResponse struct {
	Response
	Data []Student `json:"data"`
}

func fetchStudents() ([]Student, error) {
	var err error
	var client = &http.Client{}

	response, err := client.Get(baseURL + "/students")
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	var result fetchResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}

	return result.Data, nil
}

func addStudent(newStudent Student) (Response, error) {
	var client = &http.Client{}
	var resp Response
	var param = url.Values{}
	param.Set("id", newStudent.ID)
	param.Set("name", newStudent.Name)
	param.Set("age", strconv.Itoa(newStudent.Age))
	param.Set("grade", strconv.Itoa(newStudent.Grade))

	var payload = bytes.NewBufferString(param.Encode())

	response, err := client.Post(baseURL+"/students", "application/x-www-form-urlencoded", payload)
	if err != nil {
		return resp, err
	}

	defer response.Body.Close()

	err = json.NewDecoder(response.Body).Decode(&resp)
	return resp, nil
}

func removeStudent(id string) (Response, error) {
	var err error
	var client = &http.Client{}
	var resp Response

	request, err := http.NewRequest("DELETE", baseURL+"/students?id="+id, nil)
	if err != nil {
		return resp, err
	}

	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	response, err := client.Do(request)
	if err != nil {
		return resp, err
	}
	defer response.Body.Close()

	err = json.NewDecoder(response.Body).Decode(&resp)
	return resp, nil
}

func main() {
	students, err := fetchStudents()
	if err != nil {
		fmt.Println("Error!", err.Error())
		return
	}
	fmt.Printf("%+v\n", students)

	var newStudent = Student{
		ID:    "L001",
		Name:  "Leslie Pham",
		Age:   21,
		Grade: 3,
	}
	resp, err := addStudent(newStudent)
	if err != nil {
		fmt.Println("Error!", err.Error())
		return
	}
	fmt.Printf("%+v\n", resp)

	studentID := "C001"
	resp, err = removeStudent(studentID)
	if err != nil {
		fmt.Println("Error!", err.Error())
		return
	}
	fmt.Printf("%+v\n", resp)
}
