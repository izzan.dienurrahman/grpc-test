Prerequisites:
go get -u github.com/golang/protobuf/protoc-gen-go
go get -u github.com/golang/protobuf@v1.3.2
go get -u google.golang.org/grpc@v1.27.0

Deskripsi:
- Terdapat 1 client dan 2 service yaitu service broker (service A) dan service student (service B) 
- Client terhubung dengan service A melalui protocol http menggunakan restAPI sedangkan service A meneruskan permintaan ke service B yang terhubung langsung ke database.

Folder:
- client berisikan file untuk pengetesan grpc
- config berisikan informasi shared atau global, yang digunakan aplikasi client maupun server.
- model berisi proto file dan generated protobuf nya
- config berisi port number untuk service student (service B)

Test rpc client-server:
- buat database bernama db_belajar_golang di postgresql (jika belum)
- jalankan file main di terminal terpisah pada service-student, service-broker, dan terakhir client.
- untuk pengetesan dengan postman tidak perlu menjalankan file client (hanya service-broker dan service-student saja), cukup masukan url localhost:8080/students kemudian tes dengan berbagai request method.
- GET untuk mengambil semua data student
- POST untuk mendaftarkan student baru
- DELETE untuk menghapus student terdaftar

ARGUMEN API UNTUK POSTMAN:
- POST : id(varchar 5), name(varchar 255), age (int), grade (int)
- DELETE: id(varchar 5)
- GET: tidak perlu argumen

Rujukan tutorial : https://dasarpemrogramangolang.novalagung.com/C-30-golang-grpc-protobuf.html
