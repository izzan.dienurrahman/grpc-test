package main

import (
	"context"
	"encoding/json"
	"fmt"
	"go-gRPC-proto/common/config"
	"go-gRPC-proto/common/model"
	"log"
	"net/http"
	"strconv"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc"
)

var response map[string]interface{}

// service B or student service
func serviceStudent() model.StudentsClient {
	port := config.SERVICE_STUDENT_PORT
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatal("could not connect to", port, err)
	}
	return model.NewStudentsClient(conn)
}

func headers(w http.ResponseWriter, req *http.Request) {

	for name, headers := range req.Header {
		for _, h := range headers {
			fmt.Fprintf(w, "%v: %v\n", name, h)
		}
	}
}

func students(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case "POST":
		w.Header().Set("Content-Type", "application/json")
		// Convert some data to appropriate form
		age, _ := strconv.ParseInt(req.FormValue("age"), 10, 32)
		grade, _ := strconv.ParseInt(req.FormValue("grade"), 10, 32)

		// Reconstruct param student before calling to service B via gRPC
		student := model.Student{
			Id:    req.FormValue("id"),
			Name:  req.FormValue("name"),
			Age:   int32(age),
			Grade: int32(grade),
		}

		// Call service B
		serviceB := serviceStudent()
		_, err := serviceB.AddStudent(context.Background(), &student)
		if err != nil {
			log.Println(err)
			response = map[string]interface{}{
				"success":    false,
				"message":    "ID already exist!",
				"error_code": 400,
			}
			jsonData, err := json.Marshal(response)
			if err != nil {
				log.Println(err)
				return
			}
			w.Write(jsonData)
			return
		}

		response = map[string]interface{}{
			"success": true,
			"message": "Add Student Success!",
		}

		jsonData, err := json.Marshal(response)
		w.Write(jsonData)
		return

	case "GET":
		w.Header().Set("Content-Type", "application/json")
		serviceB := serviceStudent()
		studentList, err := serviceB.ShowAllStudent(context.Background(), new(empty.Empty))
		if err != nil {
			response = map[string]interface{}{
				"success":    false,
				"message":    "Resource Not Found!",
				"error_code": 404,
			}
			return
		}

		response = map[string]interface{}{
			"success": true,
			"message": "Show All Student Success!",
			"data":    studentList.List,
		}

		jsonData, err := json.Marshal(response)
		if err != nil {
			log.Println(err)
			return
		}
		w.Write(jsonData)
		return

	case "DELETE":
		w.Header().Set("Content-Type", "application/json")
		studentID := model.StudentID{
			Id: req.FormValue("id"),
		}
		serviceB := serviceStudent()
		_, err := serviceB.RemoveStudent(context.Background(), &studentID)
		if err != nil {
			log.Println(err)
			response = map[string]interface{}{
				"success":    false,
				"message":    "Bad Request, ID does not exist!",
				"error_code": 400,
			}
			jsonData, err := json.Marshal(response)
			if err != nil {
				log.Println(err)
				return
			}
			w.Write(jsonData)
			return
		}
		response = map[string]interface{}{
			"success": true,
			"message": "Student Removed Successfully!",
		}
		jsonData, err := json.Marshal(response)
		if err != nil {
			log.Println(err)
			return
		}
		w.Write(jsonData)
		return

	default:
		http.Error(w, "", http.StatusBadRequest)
	}
}

func main() {
	http.HandleFunc("/students", students)
	http.HandleFunc("/headers", headers)

	http.ListenAndServe(":8080", nil)
}
