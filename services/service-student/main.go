package main

import (
	"context"
	"errors"
	"fmt"
	"go-gRPC-proto/common/config"
	"go-gRPC-proto/common/model"
	"log"
	"net"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jackc/pgx/v4"
	"google.golang.org/grpc"
)

var allStudents *model.StudentList

func init() {
	allStudents = new(model.StudentList)
	allStudents.List = make([]*model.Student, 0)
}

// StudentsServer is exported
type StudentsServer struct{}

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "1234"
	dbname   = "db_belajar_golang"
)

var psqlInfo = fmt.Sprintf("host=%s port=%d user=%s "+
	"password=%s dbname=%s sslmode=disable",
	host, port, user, password, dbname)

// AddStudent is exported
func (StudentsServer) AddStudent(ctx context.Context, param *model.Student) (*empty.Empty, error) {
	conn, err := pgx.Connect(ctx, psqlInfo)
	if err != nil {
		return nil, err
	}
	defer conn.Close(ctx)

	_, err = conn.Exec(ctx, "INSERT INTO tb_student values ($1, $2, $3, $4)", param.Id, param.Name, param.Age, param.Grade)
	if err != nil {
		log.Printf("Insert attempt failed! id:%s name:%s age:%d grade:%d\n", param.Id, param.Name, param.Age, param.Grade)
		return new(empty.Empty), err
	}
	log.Printf("Insert operation success! id:%s name:%s age:%d grade:%d\n", param.Id, param.Name, param.Age, param.Grade)

	return new(empty.Empty), nil
}

// RemoveStudent is exported
func (StudentsServer) RemoveStudent(ctx context.Context, param *model.StudentID) (*empty.Empty, error) {

	conn, err := pgx.Connect(ctx, psqlInfo)
	if err != nil {
		return nil, err
	}
	defer conn.Close(ctx)
	ctag, err := conn.Exec(ctx, "DELETE FROM tb_student WHERE id = $1", param.Id)

	if ctag.String() == "DELETE 0" {
		log.Printf("Delete attempt failed! id:%s\n", param.Id)
		return new(empty.Empty), errors.New("ID does not exist")
	}

	if err != nil {
		return new(empty.Empty), err
	}
	log.Printf("Delete operation success! id:%s\n", param.Id)

	return new(empty.Empty), nil
}

// ShowAllStudent is exported
func (StudentsServer) ShowAllStudent(ctx context.Context, void *empty.Empty) (*model.StudentList, error) {
	conn, err := pgx.Connect(ctx, psqlInfo)
	if err != nil {
		return nil, err
	}
	defer conn.Close(ctx)

	rows, err := conn.Query(ctx, "SELECT * from tb_student")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var listMurid *model.StudentList
	listMurid = new(model.StudentList)
	listMurid.List = make([]*model.Student, 0)
	for rows.Next() {
		var each model.Student
		var err = rows.Scan(&each.Id, &each.Name, &each.Age, &each.Grade)

		if err != nil {
			return nil, err
		}

		listMurid.List = append(listMurid.List, &each)
	}
	log.Printf("Select operation success! id:*\n")
	return listMurid, nil
}

func main() {
	srv := grpc.NewServer()
	var studentSrv StudentsServer
	model.RegisterStudentsServer(srv, studentSrv)

	log.Println("Starting RPC server at", config.SERVICE_STUDENT_PORT)

	// more code here ...
	l, err := net.Listen("tcp", config.SERVICE_STUDENT_PORT)
	if err != nil {
		log.Fatalf("could not listen to %s: %v", config.SERVICE_STUDENT_PORT, err)
	}

	log.Fatal(srv.Serve(l))
}
